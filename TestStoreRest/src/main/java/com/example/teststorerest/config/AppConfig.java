package com.example.teststorerest.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@Configuration
@ComponentScan("com.example.teststorerest")
@PropertySource(value = {"classpath:testStoreRest.properties"},
        ignoreResourceNotFound = true)
public class AppConfig {

}
