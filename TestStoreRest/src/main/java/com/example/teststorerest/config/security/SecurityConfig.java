package com.example.teststorerest.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    public void authConfigure(
            AuthenticationManagerBuilder auth,
            PasswordEncoder encoder,
            UserAuthService userAuthService
    ) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password(encoder.encode("admin"))
                .roles("ADMIN");

        auth.inMemoryAuthentication()
                .withUser("user")
                .password(encoder.encode("user"))
                .roles("GUEST");

        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userAuthService);
        provider.setPasswordEncoder(encoder);
        auth.authenticationProvider(provider);
    }

    private final String[] usersResources = new String[]{
            "/user/account", "/user/create", "/purchase/currentUserPurchases", "/product/id/**",
            "/product/productName/**", "/product/price/**", "/product/keyword/**", "/product/tags/**",
            "/product/all", "/product/all", "/product/setEstimate/**", "/product/setReview/**",
            "/product/buyProduct/**", "/organization/id/**", "/organization/organizationName/**",
            "/organization/organizationName/**", "/organization/all", "/organization/all",
            "/organization/all", "/organization/createRequest", "/organization/createRequest",
            "/message/currentUserMessages"
    };

    private final String[] usersFrozenResources = new String[]{
            "/purchase/currentUserPurchases", "/product/id/**",
            "/product/productName/**", "/product/price/**", "/product/keyword/**", "/product/tags/**",
            "/product/all", "/product/all"
    };

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/")
                .hasAnyRole("ADMIN", "USER", "USER_ORG", "GUEST")
                .antMatchers("/**")
                .hasAnyRole("ADMIN")
                .antMatchers(usersResources)
                .hasAnyRole("USER", "USER_ORG")
                .antMatchers("/user/create")
                .hasAnyRole("GUEST")
                .antMatchers("/product/createRequest")
                .hasAnyRole("USER_ORG")
                .antMatchers(usersFrozenResources)
                .hasAnyRole("USER_FROZEN")
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .permitAll()
                .and()
                .httpBasic()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
        return http.build();
    }
}
