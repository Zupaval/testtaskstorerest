package com.example.teststorerest.controller;

import com.example.teststorerest.model.dto.MessageDto;
import com.example.teststorerest.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/message")
public class MessageController {

    MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/id/{id}")
    MessageDto findMessageById(@PathVariable Long id) {
        return messageService.findMessageById(id);
    }

    @GetMapping("/currentUserMessages")
    List<MessageDto> findMessagesCurrentUser() {
        return messageService.findMessagesCurrentUser();
    }

    @GetMapping("/all")
    List<MessageDto> findAllMessages() {
        return messageService.findAllMessages();
    }

    @PostMapping("/create/{id}")
    MessageDto createMessageToUser(@PathVariable Long id, String header, String messageBody) {
        return messageService.createMessageToUser(id, header, messageBody);
    }
}
