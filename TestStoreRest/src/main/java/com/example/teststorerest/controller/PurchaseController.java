package com.example.teststorerest.controller;

import com.example.teststorerest.model.dto.PurchaseDto;
import com.example.teststorerest.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/purchase")
public class PurchaseController {

    private final PurchaseService purchaseService;

    @Autowired
    public PurchaseController(PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }

    @GetMapping("/id/{id}")
    PurchaseDto findPurchaseById(@PathVariable Long id) {
        return purchaseService.findPurchaseById(id);
    }

    @GetMapping("/currentUserPurchases")
    List<PurchaseDto> findPurchasesCurrentUser() {
        return purchaseService.findPurchasesCurrentUser();
    }

    @GetMapping("/all")
    List<PurchaseDto> findAllPurchases(){
        return purchaseService.findAllPurchases();
    }
}
