package com.example.teststorerest.controller;

import com.example.teststorerest.model.dto.OrganizationDto;
import com.example.teststorerest.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/organization")
public class OrganizationController {

    OrganizationService organizationService;

    @Autowired
    public OrganizationController(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    @GetMapping("/id/{id}")
    public OrganizationDto findOrganizationById(@PathVariable Long id) {
        return organizationService.findOrganizationById(id);
    }

    @GetMapping("/organizationName/{organizationName}")
    public List<OrganizationDto> findOrganizationByName(@PathVariable String organizationName) {
        return organizationService.findOrganizationByName(organizationName);
    }

    @GetMapping("/all")
    public List<OrganizationDto> findAllOrganizations() {
        return organizationService.findAllOrganizations();
    }

    @PutMapping("/executeRequest/{id}")
    public OrganizationDto executeRequestOrganization(@PathVariable Long id) {
        return organizationService.executeRequestOrganization(id);
    }

    @PostMapping("/createRequest")
    public OrganizationDto requestSaveOrganization(@RequestBody OrganizationDto organizationDto) {
        return organizationService.requestSaveOrganization(organizationDto);
    }

    @PutMapping("/freeze/{id}")
    public OrganizationDto freezeOrganizationById(@PathVariable Long id) {
        return organizationService.freezeOrganizationById(id);
    }

    @DeleteMapping("/delete/{id}")
    public OrganizationDto deleteOrganizationById(@PathVariable Long id) {
        return organizationService.deleteOrganizationById(id);
    }
}
