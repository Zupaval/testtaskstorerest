package com.example.teststorerest.controller;

import com.example.teststorerest.model.dto.UserDto;
import com.example.teststorerest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/id/{id}")
    public UserDto findUserById(@PathVariable Long id) {
        return userService.findUserById(id);
    }

    @GetMapping("/email/{email}")
    public UserDto findUserByEmail(@PathVariable String email) {
        return userService.findUserByEmail(email);
    }

    @GetMapping("/all")
    public List<UserDto> findAllUsers() {
        return userService.findAllUsers();
    }

    @PostMapping("/create")
    public UserDto createUser(@RequestBody UserDto userDto) {
        return userService.saveUser(userDto);
    }

    @PutMapping("/updateRole/{userId}")
    public UserDto updateUserRole(@PathVariable Long userId, Long roleId) {
        return userService.updateUserRole(userId, roleId);
    }

    @PutMapping("/freeze/{id}")
    public UserDto freezeUserById(@PathVariable Long id) {
        return userService.freezeUserById(id);
    }

    @DeleteMapping("/delete/{id}")
    public UserDto deleteUserById(@PathVariable Long id) {
        return userService.deleteUserById(id);
    }

    @GetMapping("/account")
    public UserDto myAccount() {
        return userService.findCurrentUser();
    }

    @PutMapping("/balance/addBalance/{id}")
    public UserDto addBalance(@PathVariable Long id, Integer amount) {
        return userService.addBalance(id, amount);
    }

    @PutMapping("/balance/subtractBalance/{id}")
    public UserDto subtractBalance(@PathVariable Long id, Integer amount) {
        return userService.subtractBalance(id, amount);
    }
}
