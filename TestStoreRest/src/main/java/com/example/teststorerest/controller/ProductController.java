package com.example.teststorerest.controller;

import com.example.teststorerest.model.dto.*;
import com.example.teststorerest.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/id/{id}")
    public ProductDto findProductById(@PathVariable Long id) {
        return productService.findProductById(id);
    }

    @GetMapping("/productName/{productName}")
    public List<ProductDto> findProductsByProductName(@PathVariable String productName) {
        return productService.findProductsByProductName(productName);
    }

    @GetMapping("/price/less/{maxPrice}")
    public List<ProductDto> findProductsByPriceLowerMax(@PathVariable Integer maxPrice) {
        return productService.findProductsByPriceLowerMax(maxPrice);
    }

    @GetMapping("/price/more/{minPrice}")
    public List<ProductDto> findProductsByPriceAboveMin(@PathVariable Integer minPrice) {
        return productService.findProductsByPriceAboveMin(minPrice);
    }

    @GetMapping("/keyword/{keyword}")
    public List<ProductDto> findProductsByKeywordOnProductName(@PathVariable String keyword) {
        return productService.findProductsByKeywordOnProductName(keyword);
    }

    @GetMapping("/tags/{tags}")
    public List<ProductDto> findProductByTags(@PathVariable List<String> tags) {
        return productService.findProductByTags(tags);
    }

    @GetMapping("/all")
    public List<ProductDto> findAllProducts() {
        return productService.findAllProducts();
    }

    @PostMapping("/createRequest")
    public ProductDto requestSaveProduct(@RequestBody ProductDto productDto) {
        return productService.requestSaveProduct(productDto);
    }

    @PutMapping("/update")
    public ProductDto updateProduct(@RequestBody ProductDto productDto) {
        return productService.updateProduct(productDto);
    }

    @DeleteMapping("/delete/{id}")
    public ProductDto deleteProductById(@PathVariable Long id) {
        return productService.deleteProductById(id);
    }

    @PutMapping("/freeze/{id}")
    public ProductDto freezeProductById(@PathVariable Long id) {
        return productService.freezeProductById(id);
    }

    @PutMapping("/executeRequest/{id}")
    public ProductDto executeRequestProduct(@PathVariable Long id) {
        return productService.executeRequestProduct(id);
    }

    @PutMapping("/setDiscount/{id}")
    public ProductDto setDiscountOnProductById(@PathVariable Long id,@RequestBody DiscountDto discountDto) {
        return productService.setDiscountOnProductById(id, discountDto);
    }

    @PutMapping("/setEstimate/{id}")
    public ProductDto setEstimateOnProduct(@PathVariable Long id,@RequestBody EstimateDto estimateDto) {
        return productService.setEstimateOnProduct(id, estimateDto);
    }

    @PutMapping("/setReview/{id}")
    public ProductDto setReviewOnProduct(@PathVariable Long id,@RequestBody ReviewDto reviewDto) {
        return productService.setReviewOnProduct(id, reviewDto);
    }

    @PostMapping("/buyProduct/{id}")
    public PurchaseDto buyProduct(@PathVariable Long id) {
        return productService.buyProduct(id);
    }
}
