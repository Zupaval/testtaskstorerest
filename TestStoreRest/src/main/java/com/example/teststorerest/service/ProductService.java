package com.example.teststorerest.service;

import com.example.teststorerest.model.dto.*;

import java.util.List;

public interface ProductService {

    ProductDto findProductById(Long id);

    List<ProductDto> findProductsByProductName(String productName);

    List<ProductDto> findProductsByPriceLowerMax(Integer maxPrice);

    List<ProductDto> findProductsByPriceAboveMin(Integer minPrice);

    List<ProductDto> findProductsByKeywordOnProductName(String keyword);

    List<ProductDto> findProductByTags(List<String> tags);

    List<ProductDto> findAllProducts();

    ProductDto requestSaveProduct(ProductDto productDto);

    ProductDto updateProduct(ProductDto productDto);

    ProductDto deleteProductById(Long id);

    ProductDto freezeProductById(Long id);

    ProductDto executeRequestProduct(Long id);

    ProductDto setDiscountOnProductById(Long id, DiscountDto discountDto);

    ProductDto setEstimateOnProduct(Long id, EstimateDto estimateDto);

    ProductDto setReviewOnProduct(Long id, ReviewDto reviewDto);

    PurchaseDto buyProduct(Long id);
}
