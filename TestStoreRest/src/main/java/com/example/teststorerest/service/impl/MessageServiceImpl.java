package com.example.teststorerest.service.impl;

import com.example.teststorerest.config.security.CurrentUserId;
import com.example.teststorerest.model.dto.MessageDto;
import com.example.teststorerest.model.dto.mapper.MessageMapper;
import com.example.teststorerest.model.entity.MessageEntity;
import com.example.teststorerest.model.entity.UserEntity;
import com.example.teststorerest.model.repository.MessageRepository;
import com.example.teststorerest.model.repository.UserRepository;
import com.example.teststorerest.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;

    private final CurrentUserId currentUserId;

    private final UserRepository userRepository;

    @Autowired
    public MessageServiceImpl(MessageRepository messageRepository, CurrentUserId currentUserId,
                              UserRepository userRepository) {
        this.messageRepository = messageRepository;
        this.currentUserId = currentUserId;
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public MessageDto findMessageById(Long id) {

        MessageEntity messageEntity = messageRepository.findById(id).orElseGet(MessageEntity::new);
        return MessageMapper.fromEntityToDto(messageEntity);
    }

    @Transactional
    @Override
    public List<MessageDto> findMessagesCurrentUser() {

        UserEntity userEntity = userRepository.findById(currentUserId.getUserId()).orElseThrow();
        List<MessageEntity> messages = messageRepository.findAllByUser(userEntity);
        return messages.stream().map(MessageMapper::fromEntityToDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<MessageDto> findAllMessages() {
        List<MessageEntity> messages = messageRepository.findAll();
        return messages.stream().map(MessageMapper::fromEntityToDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public MessageDto createMessageToUser(Long id, String header, String messageBody) {

        UserEntity userEntity = userRepository.findById(id).orElseThrow();

        MessageEntity messageEntityTemp = MessageEntity.builder()
                .header(header)
                .messageBody(messageBody)
                .messageDate(new Date())
                .user(userEntity)
                .build();

        MessageEntity messageEntity = messageRepository.save(messageEntityTemp);
        return MessageMapper.fromEntityToDto(messageEntity);
    }
}
