package com.example.teststorerest.service;

import com.example.teststorerest.model.dto.PurchaseDto;

import java.util.List;

public interface PurchaseService {

    PurchaseDto findPurchaseById(Long id);

    List<PurchaseDto> findPurchasesCurrentUser();

    List<PurchaseDto> findAllPurchases();
}
