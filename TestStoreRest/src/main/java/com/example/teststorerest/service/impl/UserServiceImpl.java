package com.example.teststorerest.service.impl;

import com.example.teststorerest.config.security.CurrentUserId;
import com.example.teststorerest.model.dto.UserDto;
import com.example.teststorerest.model.dto.mapper.UserMapper;
import com.example.teststorerest.model.entity.RoleEntity;
import com.example.teststorerest.model.entity.UserEntity;
import com.example.teststorerest.model.repository.UserRepository;
import com.example.teststorerest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final CurrentUserId currentUserId;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder,
                           CurrentUserId currentUserId) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.currentUserId = currentUserId;
    }

    @Transactional
    @Override
    public UserDto findCurrentUser() {

        UserEntity userEntity = userRepository.findById(currentUserId.getUserId()).orElseThrow();
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto findUserById(Long id) {

        UserEntity userEntity = userRepository.findById(id).orElseGet(UserEntity::new);
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto findUserByEmail(String email) {

        UserEntity userEntity = userRepository.findUserByEmail(email).orElseGet(UserEntity::new);
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public List<UserDto> findAllUsers() {

        return userRepository.findAll()
                .stream()
                .map(UserMapper::fromEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public UserDto saveUser(UserDto userDto) {

        List<String> userEmails = userRepository.findAll()
                .stream()
                .map(UserMapper::fromEntityToDto)
                .map(UserDto::getEmail)
                .collect(Collectors.toList());

        if(userEmails.contains(userDto.getEmail())) {
            throw new RuntimeException("Такой email уже существует!");
        }
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        UserEntity userEntityTemp = UserMapper.fromDtoToEntity(userDto);
        userEntityTemp.setBalance(0);
        userEntityTemp.setRoles(List.of(new RoleEntity(2L, "USER", List.of(userEntityTemp))));
        UserEntity userEntity = userRepository.save(userEntityTemp);
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto updateUserRole(Long userId, Long roleId) {

        userRepository.findById(userId).orElseThrow();
        userRepository.removeUserRole(userId);
        userRepository.addUserRole(userId, roleId);
        UserEntity userEntity = userRepository.findById(userId).orElseThrow();
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto deleteUserById(Long id) {

        userRepository.findById(id).orElseThrow();
        userRepository.removeUserRole(id);
        userRepository.addUserRole(id, 5L);
        UserEntity userEntity = userRepository.findById(id).orElseThrow();
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto freezeUserById(Long id) {

        userRepository.findById(id).orElseThrow();
        userRepository.removeUserRole(id);
        userRepository.addUserRole(id, 4L);
        UserEntity userEntity = userRepository.findById(id).orElseThrow();
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto addBalance(Long id, Integer amount) {

        UserEntity userEntity = userRepository.findById(id).orElseGet(UserEntity::new);
        Integer balanceTemp = userEntity.getBalance();
        balanceTemp += amount;
        userEntity.setBalance(balanceTemp);
        userRepository.save(userEntity);
        return UserMapper.fromEntityToDto(userEntity);
    }

    @Transactional
    @Override
    public UserDto subtractBalance(Long id, Integer amount) {

        UserEntity userEntity = userRepository.findById(id).orElseGet(UserEntity::new);
        Integer balanceTemp = userEntity.getBalance();
        balanceTemp -= amount;
        userEntity.setBalance(balanceTemp);
        userRepository.save(userEntity);
        return UserMapper.fromEntityToDto(userEntity);
    }
}
