package com.example.teststorerest.service;

import com.example.teststorerest.model.dto.UserDto;

import java.util.List;

public interface UserService {

    UserDto findCurrentUser();

    UserDto findUserById(Long id);

    UserDto findUserByEmail(String email);

    List<UserDto> findAllUsers();

    UserDto saveUser(UserDto userDto);

    UserDto updateUserRole(Long userId, Long roleId);

    UserDto deleteUserById(Long id);

    UserDto freezeUserById(Long id);

    UserDto addBalance(Long id, Integer amount);

    UserDto subtractBalance(Long id, Integer amount);
}
