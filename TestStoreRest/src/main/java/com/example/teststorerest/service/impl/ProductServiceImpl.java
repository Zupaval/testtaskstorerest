package com.example.teststorerest.service.impl;

import com.example.teststorerest.config.security.CurrentUserId;
import com.example.teststorerest.model.dto.*;
import com.example.teststorerest.model.dto.mapper.*;
import com.example.teststorerest.model.entity.*;
import com.example.teststorerest.model.repository.*;
import com.example.teststorerest.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final StatusRepository statusRepository;

    private final TagRepository tagRepository;

    private final PurchaseRepository purchaseRepository;

    private final CurrentUserId currentUserId;

    private final UserRepository userRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, StatusRepository statusRepository,
                              TagRepository tagRepository, PurchaseRepository purchaseRepository,
                              CurrentUserId currentUserId, UserRepository userRepository) {
        this.productRepository = productRepository;
        this.statusRepository = statusRepository;
        this.tagRepository = tagRepository;
        this.purchaseRepository = purchaseRepository;
        this.currentUserId = currentUserId;
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public ProductDto findProductById(Long id) {
        ProductEntity productEntity = productRepository.findById(id).orElseGet(ProductEntity::new);
        if (productEntity.getStatus().getId() != 1) {
            return new ProductDto();
        }
        return ProductMapper.fromEntityToDto(productEntity);
    }

    @Transactional
    @Override
    public List<ProductDto> findProductsByProductName(String productName) {

        List<ProductEntity> products = productRepository.findAllByProductName(productName)
                .stream()
                .filter(p -> p.getStatus().getId() == 1)
                .collect(Collectors.toList());

        return products.stream().map(ProductMapper::fromEntityToDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<ProductDto> findProductsByPriceLowerMax(Integer maxPrice) {

        List<ProductEntity> products = productRepository.findProductsByPriceLowerMax(maxPrice)
                .stream()
                .filter(p -> p.getStatus().getId() == 1)
                .collect(Collectors.toList());
        return products.stream().map(ProductMapper::fromEntityToDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<ProductDto> findProductsByPriceAboveMin(Integer minPrice) {

        List<ProductEntity> products = productRepository.findProductsByPriceAboveMin(minPrice)
                .stream()
                .filter(p -> p.getStatus().getId() == 1)
                .collect(Collectors.toList());
        return products.stream().map(ProductMapper::fromEntityToDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<ProductDto> findProductsByKeywordOnProductName(String keyword) {

        keyword = "%" + keyword + "%";

        List<ProductEntity> products = productRepository.findProductsByKeyword(keyword)
                .stream()
                .filter(p -> p.getStatus().getId() == 1)
                .collect(Collectors.toList());
        return products.stream().map(ProductMapper::fromEntityToDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<ProductDto> findProductByTags(List<String> tags) {

        List<ProductEntity> productsTemp = new ArrayList<>();

        tags.forEach(tag -> tagRepository.findAllByTag(tag)
                .forEach(tagEntity -> productsTemp.add(tagEntity.getProduct())));

        List<ProductEntity> products = productsTemp.stream()
                .filter(p -> p.getStatus().getId() == 1)
                .collect(Collectors.toList());

        return products.stream().map(ProductMapper::fromEntityToDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<ProductDto> findAllProducts() {

        List<ProductEntity> products = productRepository.findAll()
                .stream()
                .filter(p -> p.getStatus().getId() == 1)
                .collect(Collectors.toList());
        return products.stream().map(ProductMapper::fromEntityToDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public ProductDto requestSaveProduct(ProductDto productDto) {

        ProductEntity productEntityTemp = ProductMapper.fromDtoToEntity(productDto);
        StatusEntity statusEntity = statusRepository.findById(4L).orElseThrow();
        productEntityTemp.setStatus(statusEntity);
        ProductEntity productEntity = productRepository.save(productEntityTemp);
        return ProductMapper.fromEntityToDto(productEntity);
    }

    @Transactional
    @Override
    public ProductDto updateProduct(ProductDto productDto) {

        ProductEntity productEntityTemp = ProductMapper.fromDtoToEntity(productDto);
        ProductEntity productEntity = productRepository.saveAndFlush(productEntityTemp);
        return ProductMapper.fromEntityToDto(productEntity);
    }

    @Transactional
    @Override
    public ProductDto deleteProductById(Long id) {

        ProductEntity productEntityTemp = productRepository.findById(id).orElseThrow();
        StatusEntity statusEntity = statusRepository.findById(3L).orElseThrow();
        productEntityTemp.setStatus(statusEntity);
        ProductEntity productEntity = productRepository.saveAndFlush(productEntityTemp);
        return ProductMapper.fromEntityToDto(productEntity);
    }

    @Transactional
    @Override
    public ProductDto freezeProductById(Long id) {

        ProductEntity productEntityTemp = productRepository.findById(id).orElseThrow();
        StatusEntity statusEntity = statusRepository.findById(2L).orElseThrow();
        productEntityTemp.setStatus(statusEntity);
        ProductEntity productEntity = productRepository.saveAndFlush(productEntityTemp);
        return ProductMapper.fromEntityToDto(productEntity);
    }

    @Transactional
    @Override
    public ProductDto executeRequestProduct(Long id) {

        ProductEntity productEntityTemp = productRepository.findById(id).orElseThrow();
        StatusEntity statusEntity = statusRepository.findById(1L).orElseThrow();
        productEntityTemp.setStatus(statusEntity);
        ProductEntity productEntity = productRepository.saveAndFlush(productEntityTemp);
        return ProductMapper.fromEntityToDto(productEntity);
    }

    @Transactional
    @Override
    public ProductDto setDiscountOnProductById(Long id, DiscountDto discountDto) {

        ProductEntity productEntityTemp = productRepository.findById(id).orElseThrow();
        productEntityTemp.setDiscount(DiscountMapper.fromDtoToEntity(discountDto));
        ProductEntity productEntity = productRepository.saveAndFlush(productEntityTemp);
        return ProductMapper.fromEntityToDto(productEntity);
    }

    @Transactional
    @Override
    public ProductDto setEstimateOnProduct(Long id, EstimateDto estimateDto) {

        ProductEntity productEntityTemp = productRepository.findById(id).orElseThrow();
        UserEntity userEntity = userRepository.findById(currentUserId.getUserId()).orElseThrow();
        PurchaseEntity purchaseEntity = purchaseRepository.findByUserAndProduct(userEntity, productEntityTemp);
        if (purchaseEntity == null) {
            return new ProductDto();
        }

        List<EstimateEntity> estimates = productEntityTemp.getEstimates();
        estimates.add(EstimateMapper.fromDtoToEntity(estimateDto));
        productEntityTemp.setEstimates(estimates);
        ProductEntity productEntity = productRepository.saveAndFlush(productEntityTemp);
        return ProductMapper.fromEntityToDto(productEntity);
    }

    @Transactional
    @Override
    public ProductDto setReviewOnProduct(Long id, ReviewDto reviewDto) {

        ProductEntity productEntityTemp = productRepository.findById(id).orElseThrow();
        UserEntity userEntity = userRepository.findById(currentUserId.getUserId()).orElseThrow();
        PurchaseEntity purchaseEntity = purchaseRepository.findByUserAndProduct(userEntity, productEntityTemp);
        if (purchaseEntity == null) {
            return new ProductDto();
        }

        List<ReviewEntity> reviews = productEntityTemp.getReviews();
        reviews.add(ReviewMapper.fromDtoToEntity(reviewDto));
        productEntityTemp.setReviews(reviews);
        ProductEntity productEntity = productRepository.saveAndFlush(productEntityTemp);
        return ProductMapper.fromEntityToDto(productEntity);
    }

    @Transactional
    @Override
    public PurchaseDto buyProduct(Long id) {

        UserEntity userEntityConsumer = userRepository.findById(currentUserId.getUserId()).orElseThrow();
        ProductEntity productEntity = productRepository.findById(id).orElseThrow();
        Integer price = productEntity.getPrice();
        Integer quantity = productEntity.getQuantity();
        Integer discount = productEntity.getDiscount().getDiscount_amount();
        UserEntity userEntityProducer = productEntity.getOrganization().getUser();
        Integer producerBalance = userEntityProducer.getBalance();
        Integer consumerBalance = userEntityConsumer.getBalance();

        Date duration = productEntity.getDiscount().getDiscount_duration();
        Date dateNow = new Date();
        if (duration.after(dateNow)) {

            price = price - (price * discount) / 100;
        }

        Integer newConsumerBalance = consumerBalance - price;

        if ((newConsumerBalance) < 0 || quantity <= 0) {

            return new PurchaseDto();
        }
        userEntityConsumer.setBalance(newConsumerBalance);
        userRepository.saveAndFlush(userEntityConsumer);

        Integer commission = 5;
        Integer newProducerBalance = producerBalance + price - (price * commission) / 100;
        userEntityProducer.setBalance(newProducerBalance);
        userRepository.saveAndFlush(userEntityProducer);

        productEntity.setQuantity(quantity - 1);
        productRepository.saveAndFlush(productEntity);
        PurchaseEntity purchaseEntityTemp = PurchaseEntity.builder()
                .purchaseDate(dateNow)
                .user(userEntityConsumer)
                .product(productEntity)
                .purchaseReturn(false)
                .purchasePrice(price)
                .build();
        PurchaseEntity purchaseEntity = purchaseRepository.saveAndFlush(purchaseEntityTemp);
        return PurchaseMapper.fromEntityToDto(purchaseEntity);
    }
}
