package com.example.teststorerest.service.impl;

import com.example.teststorerest.config.security.CurrentUserId;
import com.example.teststorerest.model.dto.PurchaseDto;
import com.example.teststorerest.model.dto.mapper.PurchaseMapper;
import com.example.teststorerest.model.entity.PurchaseEntity;
import com.example.teststorerest.model.entity.UserEntity;
import com.example.teststorerest.model.repository.PurchaseRepository;
import com.example.teststorerest.model.repository.UserRepository;
import com.example.teststorerest.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PurchaseServiceImpl implements PurchaseService {

    private final PurchaseRepository purchaseRepository;

    private final CurrentUserId currentUserId;

    private final UserRepository userRepository;

    @Autowired
    public PurchaseServiceImpl(PurchaseRepository purchaseRepository, CurrentUserId currentUserId, UserRepository userRepository) {
        this.purchaseRepository = purchaseRepository;
        this.currentUserId = currentUserId;
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public PurchaseDto findPurchaseById(Long id) {

        PurchaseEntity purchaseEntityTemp = purchaseRepository.findById(id).orElseThrow();
        return PurchaseMapper.fromEntityToDto(purchaseEntityTemp);
    }

    @Transactional
    @Override
    public List<PurchaseDto> findPurchasesCurrentUser() {

        UserEntity userEntity = userRepository.findById(currentUserId.getUserId()).orElseThrow();
        List<PurchaseEntity> purchases = purchaseRepository.findAllByUser(userEntity);
        return purchases.stream().map(PurchaseMapper::fromEntityToDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<PurchaseDto> findAllPurchases() {

        List<PurchaseEntity> purchases = purchaseRepository.findAll();
        return purchases.stream().map(PurchaseMapper::fromEntityToDto).collect(Collectors.toList());
    }
}
