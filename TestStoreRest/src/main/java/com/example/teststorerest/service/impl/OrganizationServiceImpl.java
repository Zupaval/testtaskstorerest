package com.example.teststorerest.service.impl;

import com.example.teststorerest.model.dto.OrganizationDto;
import com.example.teststorerest.model.dto.mapper.OrganizationMapper;
import com.example.teststorerest.model.entity.OrganizationEntity;
import com.example.teststorerest.model.entity.StatusEntity;
import com.example.teststorerest.model.repository.OrganizationRepository;
import com.example.teststorerest.model.repository.StatusRepository;
import com.example.teststorerest.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class OrganizationServiceImpl implements OrganizationService {


    private final OrganizationRepository organizationRepository;

    private final StatusRepository statusRepository;

    @Autowired
    public OrganizationServiceImpl(OrganizationRepository organizationRepository, StatusRepository statusRepository) {
        this.organizationRepository = organizationRepository;
        this.statusRepository = statusRepository;
    }

    @Transactional
    @Override
    public OrganizationDto findOrganizationById(Long id) {

        OrganizationEntity organizationEntity = organizationRepository.findById(id).orElseGet(OrganizationEntity::new);
        return OrganizationMapper.fromEntityToDto(organizationEntity);
    }

    @Transactional
    @Override
    public List<OrganizationDto> findOrganizationByName(String organizationName) {

        List<OrganizationEntity> organizations = organizationRepository.findAllByOrganizationName(organizationName);
        return organizations.stream().map(OrganizationMapper::fromEntityToDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public List<OrganizationDto> findAllOrganizations() {

        List<OrganizationEntity> organizations = organizationRepository.findAll();
        return organizations.stream().map(OrganizationMapper::fromEntityToDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public OrganizationDto executeRequestOrganization(Long id) {

        OrganizationEntity organizationEntityTemp = organizationRepository.findById(id).orElseThrow();
        StatusEntity statusEntity = statusRepository.findById(1L).orElseThrow();
        organizationEntityTemp.setStatus(statusEntity);
        OrganizationEntity organizationEntity = organizationRepository.saveAndFlush(organizationEntityTemp);
        return OrganizationMapper.fromEntityToDto(organizationEntity);
    }

    @Transactional
    @Override
    public OrganizationDto requestSaveOrganization(OrganizationDto organizationDto) {

        OrganizationEntity organizationEntityTemp = OrganizationMapper.fromDtoToEntity(organizationDto);
        StatusEntity statusEntity = statusRepository.findById(4L).orElseThrow();
        organizationEntityTemp.setStatus(statusEntity);
        OrganizationEntity organizationEntity = organizationRepository.save(organizationEntityTemp);
        return OrganizationMapper.fromEntityToDto(organizationEntity);
    }

    @Transactional
    @Override
    public OrganizationDto freezeOrganizationById(Long id) {
        OrganizationEntity organizationEntityTemp = organizationRepository.findById(id).orElseThrow();
        StatusEntity statusEntity = statusRepository.findById(2L).orElseThrow();
        organizationEntityTemp.setStatus(statusEntity);
        OrganizationEntity organizationEntity = organizationRepository.saveAndFlush(organizationEntityTemp);
        return OrganizationMapper.fromEntityToDto(organizationEntity);
    }

    @Transactional
    @Override
    public OrganizationDto deleteOrganizationById(Long id) {
        OrganizationEntity organizationEntityTemp = organizationRepository.findById(id).orElseThrow();
        StatusEntity statusEntity = statusRepository.findById(3L).orElseThrow();
        organizationEntityTemp.setStatus(statusEntity);
        OrganizationEntity organizationEntity = organizationRepository.saveAndFlush(organizationEntityTemp);
        return OrganizationMapper.fromEntityToDto(organizationEntity);
    }
}
