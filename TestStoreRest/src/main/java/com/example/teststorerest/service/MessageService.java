package com.example.teststorerest.service;

import com.example.teststorerest.model.dto.MessageDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MessageService {

    MessageDto findMessageById(Long id);

    List<MessageDto> findMessagesCurrentUser();

    List<MessageDto> findAllMessages();

    MessageDto createMessageToUser(Long id, String header, String messageBody);
}
