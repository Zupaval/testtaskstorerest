package com.example.teststorerest.service;

import com.example.teststorerest.model.dto.OrganizationDto;

import java.util.List;

public interface OrganizationService {

    OrganizationDto findOrganizationById(Long id);

    List<OrganizationDto> findOrganizationByName(String organizationName);

    List<OrganizationDto> findAllOrganizations();

    OrganizationDto executeRequestOrganization(Long id);

    OrganizationDto requestSaveOrganization(OrganizationDto organizationDto);

    OrganizationDto freezeOrganizationById(Long id);

    OrganizationDto deleteOrganizationById(Long id);
}
