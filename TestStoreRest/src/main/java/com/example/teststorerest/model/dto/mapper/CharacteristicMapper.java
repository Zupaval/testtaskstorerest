package com.example.teststorerest.model.dto.mapper;

import com.example.teststorerest.model.dto.CharacteristicDto;
import com.example.teststorerest.model.entity.CharacteristicEntity;

public class CharacteristicMapper {

    public static CharacteristicDto fromEntityToDto(CharacteristicEntity characteristicEntity) {

        return CharacteristicDto.builder()
                .id(characteristicEntity.getId())
                .productType(characteristicEntity.getProductType())
                .productMaterial(characteristicEntity.getProductMaterial())
                .countryOrigin(characteristicEntity.getCountryOrigin())
                .build();
    }

    public static CharacteristicEntity fromDtoToEntity(CharacteristicDto characteristicDto) {

        return CharacteristicEntity.builder()
                .id(characteristicDto.getId())
                .productType(characteristicDto.getProductType())
                .productMaterial(characteristicDto.getProductMaterial())
                .countryOrigin(characteristicDto.getCountryOrigin())
                .build();
    }
}
