package com.example.teststorerest.model.repository;

import com.example.teststorerest.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findUserByEmail(String email);

    @Modifying
    @Query(value = "insert into users_roles (user_id, role_id) values (:userId, :roleId)", nativeQuery = true)
    void addUserRole(Long userId, Long roleId);

    @Modifying
    @Query(value = "delete from users_roles where user_id = :id", nativeQuery = true)
    void removeUserRole(Long id);
}
