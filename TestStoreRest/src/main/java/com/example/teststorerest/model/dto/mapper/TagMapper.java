package com.example.teststorerest.model.dto.mapper;

import com.example.teststorerest.model.dto.TagDto;
import com.example.teststorerest.model.entity.TagEntity;

public class TagMapper {

    public static TagDto fromEntityToDto(TagEntity tagEntity) {

        return TagDto.builder()
                .id(tagEntity.getId())
                .tag(tagEntity.getTag())
                .build();
    }

    public static TagEntity fromDtoToEntity(TagDto tagDto) {

        return TagEntity.builder()
                .id(tagDto.getId())
                .tag(tagDto.getTag())
                .build();
    }
}
