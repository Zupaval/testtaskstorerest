package com.example.teststorerest.model.dto.mapper;

import com.example.teststorerest.model.dto.UserDto;
import com.example.teststorerest.model.entity.UserEntity;

public class UserMapper {

    public static UserDto fromEntityToDto(UserEntity userEntity) {

        return UserDto.builder()
                .id(userEntity.getId())
                .userName(userEntity.getUserName())
                .email(userEntity.getEmail())
                .password(userEntity.getPassword())
                .balance(userEntity.getBalance())
                .build();
    }

    public static UserEntity fromDtoToEntity(UserDto userDto) {

        return UserEntity.builder()
                .id(userDto.getId())
                .userName(userDto.getUserName())
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .balance(userDto.getBalance())
                .build();
    }
}
