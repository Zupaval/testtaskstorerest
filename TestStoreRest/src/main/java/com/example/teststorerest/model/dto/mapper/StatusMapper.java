package com.example.teststorerest.model.dto.mapper;

import com.example.teststorerest.model.dto.StatusDto;
import com.example.teststorerest.model.entity.StatusEntity;

public class StatusMapper {

    public static StatusDto fromEntityToDto(StatusEntity statusEntity) {

        return StatusDto.builder()
                .id(statusEntity.getId())
                .status(statusEntity.getStatus())
                .build();
    }

    public static StatusEntity fromDtoToEntity(StatusDto statusDto) {

        return StatusEntity.builder()
                .id(statusDto.getId())
                .status(statusDto.getStatus())
                .build();
    }
}
