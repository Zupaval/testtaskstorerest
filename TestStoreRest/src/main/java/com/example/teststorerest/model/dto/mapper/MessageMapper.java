package com.example.teststorerest.model.dto.mapper;

import com.example.teststorerest.model.dto.MessageDto;
import com.example.teststorerest.model.entity.MessageEntity;

public class MessageMapper {

    public static MessageDto fromEntityToDto(MessageEntity messageEntity) {

        return MessageDto.builder()
                .id(messageEntity.getId())
                .header(messageEntity.getHeader())
                .messageDate(messageEntity.getMessageDate())
                .messageBody(messageEntity.getMessageBody())
                .user(UserMapper.fromEntityToDto(messageEntity.getUser()))
                .build();
    }

    public static MessageEntity fromDtoToEntity(MessageDto messageDto) {

        return MessageEntity.builder()
                .id(messageDto.getId())
                .header(messageDto.getHeader())
                .messageDate(messageDto.getMessageDate())
                .messageBody(messageDto.getMessageBody())
                .user(UserMapper.fromDtoToEntity(messageDto.getUser()))
                .build();
    }
}
