package com.example.teststorerest.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "statuses")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StatusEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String status;

    @JsonIgnore
    @OneToMany(mappedBy = "status")
    private List<ProductEntity> products;

    @JsonIgnore
    @OneToMany(mappedBy = "status")
    private List<OrganizationEntity> organizations;
}
