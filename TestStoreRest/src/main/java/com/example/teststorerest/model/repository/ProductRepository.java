package com.example.teststorerest.model.repository;

import com.example.teststorerest.model.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    List<ProductEntity> findAllByProductName(String productName);

    @Query(value = "select * from products where price <= :maxPrice order by price desc", nativeQuery = true)
    List<ProductEntity> findProductsByPriceLowerMax(Integer maxPrice);

    @Query(value = "select * from products where price >= :minPrice order by price", nativeQuery = true)
    List<ProductEntity> findProductsByPriceAboveMin(Integer minPrice);

    @Query(value = "select * from products where product_name like :keyword or " +
            "product_description like :keyword", nativeQuery = true)
    List<ProductEntity> findProductsByKeyword(String keyword);
}
