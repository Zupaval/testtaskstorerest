package com.example.teststorerest.model.dto.mapper;

import com.example.teststorerest.model.dto.DiscountDto;
import com.example.teststorerest.model.entity.DiscountEntity;

public class DiscountMapper {

    public static DiscountDto fromEntityToDto(DiscountEntity discountEntity) {

        return DiscountDto.builder()
                .id(discountEntity.getId())
                .discount_amount(discountEntity.getDiscount_amount())
                .discount_duration(discountEntity.getDiscount_duration())
                .build();
    }

    public static DiscountEntity fromDtoToEntity(DiscountDto discountDto) {

        return DiscountEntity.builder()
                .id(discountDto.getId())
                .discount_amount(discountDto.getDiscount_amount())
                .discount_duration(discountDto.getDiscount_duration())
                .build();
    }
}
