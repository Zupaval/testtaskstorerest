package com.example.teststorerest.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDto {

    private Long id;

    private String productName;

    private String productDescription;

    private OrganizationDto organization;

    private Integer price;

    private Integer quantity;

    private DiscountDto discount;

    private List<ReviewDto> reviews;

    private List<TagDto> tags;

    private CharacteristicDto characteristic;

    private List<EstimateDto> estimates;

    private StatusDto status;
}
