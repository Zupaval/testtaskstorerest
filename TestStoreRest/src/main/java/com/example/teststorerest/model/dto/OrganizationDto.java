package com.example.teststorerest.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrganizationDto {

    private Long id;

    private String organizationName;

    private String description;

    private String logo;

    private StatusDto status;

    private UserDto user;
}
