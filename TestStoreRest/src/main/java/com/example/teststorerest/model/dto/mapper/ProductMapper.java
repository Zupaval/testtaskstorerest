package com.example.teststorerest.model.dto.mapper;

import com.example.teststorerest.model.dto.ProductDto;
import com.example.teststorerest.model.entity.ProductEntity;

import java.util.stream.Collectors;

public class ProductMapper {

    public static ProductDto fromEntityToDto(ProductEntity productEntity) {

        return ProductDto.builder()
                .id(productEntity.getId())
                .productName(productEntity.getProductName())
                .productDescription(productEntity.getProductDescription())
                .organization(OrganizationMapper.fromEntityToDto(productEntity.getOrganization()))
                .price(productEntity.getPrice())
                .quantity(productEntity.getQuantity())
                .discount(DiscountMapper.fromEntityToDto(productEntity.getDiscount()))
                .reviews(productEntity.getReviews().stream()
                        .map(ReviewMapper::fromEntityToDto).collect(Collectors.toList()))
                .tags(productEntity.getTags().stream()
                        .map(TagMapper::fromEntityToDto).collect(Collectors.toList()))
                .characteristic(CharacteristicMapper.fromEntityToDto(productEntity.getCharacteristic()))
                .estimates(productEntity.getEstimates().stream()
                        .map(EstimateMapper::fromEntityToDto).collect(Collectors.toList()))
                .status(StatusMapper.fromEntityToDto(productEntity.getStatus()))
                .build();
    }

    public static ProductEntity fromDtoToEntity(ProductDto productDto) {

        return ProductEntity.builder()
                .id(productDto.getId())
                .productName(productDto.getProductName())
                .productDescription(productDto.getProductDescription())
                .organization(OrganizationMapper.fromDtoToEntity(productDto.getOrganization()))
                .price(productDto.getPrice())
                .quantity(productDto.getQuantity())
                .discount(DiscountMapper.fromDtoToEntity(productDto.getDiscount()))
                .reviews(productDto.getReviews().stream()
                        .map(ReviewMapper::fromDtoToEntity).collect(Collectors.toList()))
                .tags(productDto.getTags().stream()
                        .map(TagMapper::fromDtoToEntity).collect(Collectors.toList()))
                .characteristic(CharacteristicMapper.fromDtoToEntity(productDto.getCharacteristic()))
                .estimates(productDto.getEstimates().stream()
                        .map(EstimateMapper::fromDtoToEntity).collect(Collectors.toList()))
                .status(StatusMapper.fromDtoToEntity(productDto.getStatus()))
                .build();
    }
}
