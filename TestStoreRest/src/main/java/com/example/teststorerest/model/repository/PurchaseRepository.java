package com.example.teststorerest.model.repository;

import com.example.teststorerest.model.entity.ProductEntity;
import com.example.teststorerest.model.entity.PurchaseEntity;
import com.example.teststorerest.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseRepository extends JpaRepository<PurchaseEntity, Long> {

    PurchaseEntity findByUserAndProduct(UserEntity userEntity, ProductEntity productEntity);

    List<PurchaseEntity> findAllByUser(UserEntity userEntity);
}
