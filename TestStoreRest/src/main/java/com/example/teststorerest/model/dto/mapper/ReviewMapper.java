package com.example.teststorerest.model.dto.mapper;

import com.example.teststorerest.model.dto.ReviewDto;
import com.example.teststorerest.model.entity.ReviewEntity;

public class ReviewMapper {

    public static ReviewDto fromEntityToDto(ReviewEntity reviewEntity) {

        return ReviewDto.builder()
                .id(reviewEntity.getId())
                .reviewText(reviewEntity.getReviewText())
                .build();
    }

    public static ReviewEntity fromDtoToEntity(ReviewDto reviewDto) {

        return ReviewEntity.builder()
                .id(reviewDto.getId())
                .reviewText(reviewDto.getReviewText())
                .build();
    }
}
