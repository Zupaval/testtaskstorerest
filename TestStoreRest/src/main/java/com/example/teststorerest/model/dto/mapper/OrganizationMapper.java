package com.example.teststorerest.model.dto.mapper;

import com.example.teststorerest.model.dto.OrganizationDto;
import com.example.teststorerest.model.entity.OrganizationEntity;

public class OrganizationMapper {

    public static OrganizationDto fromEntityToDto(OrganizationEntity organizationEntity) {

        return OrganizationDto.builder()
                .id(organizationEntity.getId())
                .organizationName(organizationEntity.getOrganizationName())
                .description(organizationEntity.getDescription())
                .logo(organizationEntity.getLogo())
                .status(StatusMapper.fromEntityToDto(organizationEntity.getStatus()))
                .user(UserMapper.fromEntityToDto(organizationEntity.getUser()))
                .build();
    }

    public static OrganizationEntity fromDtoToEntity(OrganizationDto organizationDto) {

        return OrganizationEntity.builder()
                .id(organizationDto.getId())
                .organizationName(organizationDto.getOrganizationName())
                .description(organizationDto.getDescription())
                .logo(organizationDto.getLogo())
                .status(StatusMapper.fromDtoToEntity(organizationDto.getStatus()))
                .user(UserMapper.fromDtoToEntity(organizationDto.getUser()))
                .build();
    }
}
