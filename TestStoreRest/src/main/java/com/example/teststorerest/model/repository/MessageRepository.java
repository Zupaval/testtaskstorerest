package com.example.teststorerest.model.repository;

import com.example.teststorerest.model.entity.MessageEntity;
import com.example.teststorerest.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<MessageEntity, Long> {
    List<MessageEntity> findAllByUser(UserEntity userEntity);
}
