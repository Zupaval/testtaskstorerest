package com.example.teststorerest.model.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "products")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "product_description")
    private String productDescription;

    @ManyToOne
    @JoinColumn(name = "organization_id")
    private OrganizationEntity organization;

    private Integer price;

    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "discount_id")
    private DiscountEntity discount;

    @OneToMany(mappedBy = "product")
    private List<ReviewEntity> reviews;

    @OneToMany(mappedBy = "product")
    private List<TagEntity> tags;

    @ManyToOne
    @JoinColumn(name = "characteristic_id")
    private CharacteristicEntity characteristic;

    @OneToMany(mappedBy = "product")
    private List<EstimateEntity> estimates;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private StatusEntity status;
}
