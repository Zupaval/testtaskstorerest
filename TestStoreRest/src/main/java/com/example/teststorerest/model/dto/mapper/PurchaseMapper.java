package com.example.teststorerest.model.dto.mapper;

import com.example.teststorerest.model.dto.PurchaseDto;
import com.example.teststorerest.model.entity.PurchaseEntity;

public class PurchaseMapper {

    public static PurchaseDto fromEntityToDto(PurchaseEntity purchaseEntity) {

        return PurchaseDto.builder()
                .id(purchaseEntity.getId())
                .purchaseDate(purchaseEntity.getPurchaseDate())
                .user(UserMapper.fromEntityToDto(purchaseEntity.getUser()))
                .product(ProductMapper.fromEntityToDto(purchaseEntity.getProduct()))
                .purchaseReturn(purchaseEntity.isPurchaseReturn())
                .purchasePrice(purchaseEntity.getPurchasePrice())
                .build();
    }

    public static PurchaseEntity fromDtoToEntity(PurchaseDto purchaseDto) {

        return PurchaseEntity.builder()
                .id(purchaseDto.getId())
                .purchaseDate(purchaseDto.getPurchaseDate())
                .user(UserMapper.fromDtoToEntity(purchaseDto.getUser()))
                .product(ProductMapper.fromDtoToEntity(purchaseDto.getProduct()))
                .purchaseReturn(purchaseDto.isPurchaseReturn())
                .purchasePrice(purchaseDto.getPurchasePrice())
                .build();
    }
}
