package com.example.teststorerest.model.dto.mapper;

import com.example.teststorerest.model.dto.EstimateDto;
import com.example.teststorerest.model.entity.EstimateEntity;

public class EstimateMapper {

    public static EstimateDto fromEntityToDto(EstimateEntity estimateEntity) {

        return EstimateDto.builder()
                .id(estimateEntity.getId())
                .estimate(estimateEntity.getEstimate())
                .build();
    }

    public static EstimateEntity fromDtoToEntity(EstimateDto estimateDto) {

        return EstimateEntity.builder()
                .id(estimateDto.getId())
                .estimate(estimateDto.getEstimate())
                .build();
    }
}
