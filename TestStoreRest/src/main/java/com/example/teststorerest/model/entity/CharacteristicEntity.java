package com.example.teststorerest.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "characteristics")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CharacteristicEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_type")
    private String productType;

    @Column(name = "product_material")
    private String productMaterial;

    @Column(name = "country_origin")
    private String countryOrigin;

    @JsonIgnore
    @OneToMany(mappedBy = "characteristic")
    private List<ProductEntity> products;
}
