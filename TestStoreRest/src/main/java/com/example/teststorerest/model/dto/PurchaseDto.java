package com.example.teststorerest.model.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PurchaseDto {

    private Long id;

    private Date purchaseDate;

    private UserDto user;

    private ProductDto product;

    boolean purchaseReturn;

    private Integer purchasePrice;
}
