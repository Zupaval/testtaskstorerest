create table users
(
    id        serial primary key,
    user_name varchar(255) not null,
    email     varchar(255) not null unique,
    password  varchar(255) not null,
    balance   integer
);

create table roles
(
    id   serial primary key,
    name varchar(255) not null
);

create table users_roles
(
    id      serial primary key,
    user_id bigint,
    role_id bigint
);

create table products
(
    id                  serial primary key,
    product_name        varchar(255) not null,
    product_description varchar(255) not null,
    organization_id     bigint       not null,
    price               integer      not null,
    quantity            integer      not null,
    discount_id         bigint       not null,
    characteristic_id   bigint       not null,
    status_id           bigint       not null
);

create table tags
(
    id         serial primary key,
    product_id bigint
);

create table discounts
(
    id                serial primary key,
    discount_amount   bigint,
    discount_duration date not null
);

create table reviews
(
    id          serial primary key,
    user_id     bigint,
    review_text varchar(255) not null,
    product_id  bigint
);

create table characteristics
(
    id               serial primary key,
    product_type     varchar(255) not null,
    product_material varchar(255) not null,
    country_origin   varchar(255) not null
);

create table estimates
(
    id         serial primary key,
    user_id    bigint,
    estimate   integer not null,
    product_id bigint
);

create table statuses
(
    id     serial primary key,
    status varchar(255) not null
);

create table purchases
(
    id              serial primary key,
    user_id         bigint,
    product_id      bigint,
    purchase_date   date    not null,
    purchase_return boolean,
    purchase_price  integer not null
);

create table organizations
(
    id                serial primary key,
    organization_name varchar(255) not null,
    description       varchar(255) not null,
    logo              varchar(255) not null,
    status_id         bigint       not null,
    user_id           bigint       not null
);

create table messages
(
    id           serial primary key,
    header       varchar(255) not null,
    message_date date         not null,
    message_body varchar(255) not null,
    user_id      bigint
);

alter table products
    add constraint organization_id
        foreign key (organization_id) references organizations (id);

alter table products
    add constraint discount_id
        foreign key (discount_id) references discounts (id);

alter table products
    add constraint characteristic_id
        foreign key (characteristic_id) references characteristics (id);

alter table products
    add constraint status_id
        foreign key (status_id) references statuses (id);

alter table tags
    add constraint product_id
        foreign key (product_id) references products (id);

alter table organizations
    add constraint status_id
        foreign key (status_id) references statuses (id);

alter table organizations
    add constraint user_id
        foreign key (user_id) references users(id);

alter table reviews
    add constraint user_id foreign key (user_id) references users (id);

alter table reviews
    add constraint product_id
        foreign key (product_id) references products (id);

alter table estimates
    add constraint user_id foreign key (user_id) references users (id);

alter table estimates
    add constraint product_id
        foreign key (product_id) references products (id);

alter table purchases
    add constraint user_id foreign key (user_id) references users (id);

alter table purchases
    add constraint product_id
        foreign key (product_id) references products (id);

alter table messages
    add constraint user_id foreign key (user_id) references users (id);